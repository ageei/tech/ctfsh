#!/usr/bin/env python3
# ==================================================================== #
#
# Copyright 2019 Philippe Gregoire <git@pgregoire.xyz>
#
# A simple shell for LXD hosts.
#
# Lists available exercises and create a new container based on the
# user's choice. Exercises are LXD container images aliased with a
# 'ctf/' prefix.
#
#
# Permission to use, copy, modify, and/or distribute this software for
# any purpose with or without fee is hereby granted, provided that the
# above copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
# WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
# AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
# DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
# PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
#
# ==================================================================== #

import binascii
import os
import pylxd
import random
import sys


PROGBASE = os.path.dirname(os.path.abspath(__file__))
BANNER   = os.path.join(PROGBASE, 'banner')

client = pylxd.Client()


def imgs():
    for img in client.images.all():
        for alias in img.aliases:
            yield alias['name']
            if 'ctf/' == alias['name'][:4]:
                yield alias['name']


def rdbase():
    return random.randint(2, 13)


def rdstr():
    return binascii.hexlify(os.urandom(16)).decode('utf-8')


def i2b(i, b):
    if not i:
        return '0'

    s = ''
    while i:
        s += '0123456789abcdefghijklmnopqrstuvwxyz'[int(i%b)]
        i = int(i/b)

    return s[::-1]


def get_img_map():
    r = {}
    b = rdbase()

    limgs = list(imgs())
    for i in range(len(limgs)):
        r['{}'.format(i2b(i, b))] = limgs[i]

    return r


def figlets():
    for ent in os.walk('/usr/share/figlet'):
        for fn in ent[2]:
            if '.flf' == fn[-4:]:
                yield fn[:-4]

    
def banner(txt):
    fonts = list(figlets())
    figlet  = fonts[random.randint(1, len(fonts)-1)]
    print('')
    # yes, command injection, but it's your system!
    # keep it quiet if figlet is not installed
    os.system('figlet -c -f "{}" "{}" 2>/dev/null'.format(figlet, txt))
    print('')


if not os.path.isfile(BANNER):
    banner(rdstr())
else:
    with open(BANNER, 'r') as fp:
        banner(fp.read())


mimg = get_img_map()
lmax = max([len(k) for k in mimg])
fmts = '  {{:<{}}}  {{}}'.format(lmax)

for k in mimg:
    v = mimg[k]
    print(fmts.format(k, v))


choice = None
for i in range(3):
    if choice:
        break
    c = input('> ')
    if c not in mimg:
        if c not in [mimg[k] for k in mimg]:
            print('error: no such exercise')
        else:
            choice = [k for k in mimg if mimg[k] == c][0]
    else:
        choice = c

if not choice:
    sys.exit(1)


n = 'ctf-' + rdstr()
config = {
    'name': n,
    'profiles': ['default'],
    'source': {
        'type': 'image',
        'alias': mimg[choice],
    },
}


container = client.containers.create(config, wait=True)
try:
    container.start(wait=True)

    p = os.fork()
    if 0 == p:
        os.execvp('lxc', ['lxc', 'exec', n, '/bin/sh'])
    else:
        os.waitid(os.P_PID, p, os.WEXITED)
finally:
    container.stop(wait=True)
    container.delete(wait=True)

# ==================================================================== #
