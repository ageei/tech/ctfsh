# ctfsh

A simple shell for the AGEEI CTF pwn exercices. It allows users to
select an exercice and launch a new container to enter.


## Features

- Prints customer banner with a random ASCII art -style using [figlet][1].
- Lists exercices using a random number base as an extra exercise.


## Installation

There are many ways this script can be used. It can, for example, be
used as a login shell, or, on a multi-user system, as a script that
users can invoke to do an exercise (if so, add sudo entries that will
allow users to launch the containers). The below method gives
approximate instructions when used as a login shell via SSH.

```
# 1. Select a username users will use to ssh to the system (e.g. `ctf`).

# 2. As root, make the that user's home directory
mkdir /home/ctf

# 3. Install the script somewhere accesible by that user.
cp shell.py /home/ctf/login.py

# 4. Make the script executable
chmod 0555 /home/ctf/login.py

# 5. Configure SSH to force execution of the script.
# (also ensure users can login with a non-empty password)
printf 'Match User ctf\n\tForceCommand /home/ctf/login.py\n' >>/etc/ssh/sshd_config

# 6. Restart sshd
/etc/init.d/ssh restart

# 7. Create the configuration directory for the lxc client
mkdir -p /home/ctf/.config/lxc

# 8. Create the user
useradd -s /home/ctf/login.py -d /home/ctf -g lxd ctf

# 9. Make sure the the lxc client can write files in that directory.
chown ctf /home/ctf/.config/lxc

# 10. Make sure it works
su -l ctf

# 10. Set a password on the account.
echo 'ctf:IWantToLearn' | chpasswd

# 11. Provide, somehow, the password to the users.
# (hint: pick a trivia answer as the password)

# 12. Support your users.
```


## Future development

- Support an arbitrary command to execute on entry.
- Support arbitrary container profiles to allow putting users under
  various resource constraints.
- Support asking for a proof-of-work to prevent DoSing the system
  with many containers. When accessible from the network, this attack
  can be mitigated by using iptables rules to limit the number of
  connexions from a particular source IP.


[1]: http://www.figlet.org/
